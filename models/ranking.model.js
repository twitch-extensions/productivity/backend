const mongoose = require('mongoose');
const rankingSchema = new mongoose.Schema({
  streamId: String,
  twitchId: String,
  name: String,
  photo: String,
  time: String
});

const ranking = mongoose.model('Ranking', rankingSchema);

module.exports = ranking;
