#!/usr/bin/env node

/**
 * Module dependencies.
 */

const fs = require('fs')
var app = require('../app');
var debug = require('debug')('backend-node:server');
var https = require('https');

/**
 * Get port from environment and store in Express.
 */

const httpsOptions = {
  //key: fs.readFileSync('./ssl/rosegm.com.co.pem'),
  cert: fs.readFileSync('./ssl/rosegm_com_co.crt'),
  passphrase: process.env.Phrase,
  key: fs.readFileSync('./ssl/rosegm_com_co.key')
}

var port = normalizePort(process.env.PORT || '9600');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = https.createServer(httpsOptions, app);

app.use((req, res, next) => {
  if(req.protocol === 'http') {
    res.redirect(301, `https://${req.headers.host}${req.url}`);
  }
  next();
});

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
