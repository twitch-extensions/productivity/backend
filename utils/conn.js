const mongoose = require('mongoose');
mongoose.set('debug', true);
const connectionStr = `mongodb://${process.env.username}:${process.env.psw}@${process.env.dbHost}:${process.env.dbPort}/twitchExt?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false`;
mongoose.connect(connectionStr,
  {useNewUrlParser: true, useUnifiedTopology: true}).then(
  () => {
    console.log('Success connection')
  },
  error => {
    console.error(error);
  }
);

const db = mongoose.connection;
db.on('error', error => {
  console.error(error);
});
db.once('open', function (){
  console.log('Connected');
});


module.exports = mongoose;
