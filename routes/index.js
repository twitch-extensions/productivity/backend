var express = require('express');
var router = express.Router();
const rankingController = require('../controllers/ranking.controller');
const userController = require('../controllers/user.controller');
const streamerController = require('../controllers/streamer.controller');

const ranking = rankingController;
const user = userController;


/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).send({message: "Welcome to Express"})
});
router.patch('/save-time', async function (req, res, next){
  const response = await ranking.saveOrUpdateUserTime(req.body);
  res.status(200).send(response)
});

router.get('/user/:id', async function (req, res,next){
  console.log('User route');
  const response = await user.getUserTwitch(req.params['id']);
  if(response.code !== 200){
    res.status(response.code).send({message: response.message, code: response.errCode});
  } else {
    res.status(response.code).send(response.data);
  }
});

router.get('/stream/', async function(req, res, next){
  const response = await streamerController.getStreamId();
  if(response.code !== 200){
    res.status(response.code).send({message: response.message, code: response.errCode});
  } else {
    res.status(response.code).send(response.data);
  }
})

module.exports = router;
