var express = require('express');

const conn = require('../utils/conn');
const ranking = require('../models/ranking.model');

class RankingController {
  constructor() {
    conn;
  }

  getTop10Rank(){

  }

  getUserTime(){

  }

  async saveOrUpdateUserTime(data){
    let rank = await ranking.findOne({twitchId: data.twitchId, streamId: data.streamID});
    console.log("result Query", rank);
    if(rank){
      rank.streamId = data.streamID;
      rank.name = data.name;
      rank.photo = data.photo;
      rank.time = parseInt(data.time) + parseInt(rank.time);
      const updateRank = await rank.save().then(
        success => {
          console.log('update', success);
          return success;
        }
      )
      return {message: 'Operacion realizada', data: updateRank}
    }else {
      let rank = new ranking();
      rank.twitchId = data.twitchId;
      rank.streamId = data.streamID;
      rank.name = data.name;
      rank.photo = data.photo;
      rank.time = parseInt(data.time);
      const newRank = await rank.save().then(
        success=>{
          return success;
        }
      ).catch(error => {
        return error;
      });
      return {message: 'Operacion realizada', data: newRank}
    }
  }
}

//export const rankingController = new RankingController();

module.exports = new RankingController();
