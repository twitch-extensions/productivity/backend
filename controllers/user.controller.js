const conn = require('../utils/conn');
const got = require('got');

class UserController{
  constructor() {
    this.clientID = process.env.CLIENTID;
    this.prvs = process.env.PRVS;
  }

  async getTwitchToken(){
    console.log('getting token');
    try{
      const {body} = await got.post(`https://id.twitch.tv/oauth2/token?client_id=${this.clientID}&client_secret=${this.prvs}&grant_type=client_credentials`)
      return {code: 200, data: JSON.parse(body)};
    }catch (error){
      console.error(error);
      return {code:500, message: 'Error getting user', errCode: 10203};
    }
  }

  async getUserTwitch(id){
    try{
      console.log('User function');
      let token = await this.getTwitchToken().then(result => {
        return result;
      });
      const getUser = await got.get(`https://api.twitch.tv/helix/users?id=${id}`,{
        headers:{
          'Client-ID' :` ${process.env.CLIENTID}`,
          'Authorization': `Bearer ${token.data.access_token}`,
          'Accept' : 'application/vdn.twitchtv.v5+json'
        }
      });

      let user = JSON.parse(getUser.body);
      console.log(user.data);
      return {
        code:200,
        data: {
          display_name: user.data[0].display_name,
          profile_image_url: user.data[0].profile_image_url
        }
      }
    }catch (error){
      console.error(error);
      return {code: 500, message: 'Error getting user data', errCode: 10204}
    }
  }

}


module.exports = new UserController();
