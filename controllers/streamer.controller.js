var express = require('express');

const got = require('got');
const userController = require('../controllers/user.controller');
let token = '';


class StreamerController{

  constructor() {
  }

  async getStreamerId(){
    try{
      let token = await userController.getTwitchToken();
      console.log('User function', token);

      const getUser = await got.get(`https://api.twitch.tv/helix/users?login=${process.env.streamer}`,{
        headers:{
          'Client-ID' :` ${process.env.CLIENTID}`,
          'Authorization': `Bearer ${token.data.access_token}`,
          'Accept' : 'application/vdn.twitchtv.v5+json'
        }
      });

      let user = JSON.parse(getUser.body);
      // console.log(user.data);
      return {
        code:200,
        data: {id: user.data[0].id}/*{
          display_name: user.data[0].display_name,
          profile_image_url: user.data[0].profile_image_url
        }*/
      }
    }catch (error){
      console.error(error);
      return {code: 500, message: 'Error getting streamer data', errCode: 10300}
    }
  }

  async getStreamId(){
    try{
      let token = await userController.getTwitchToken();
      let streamer = await this.getStreamerId();
      console.log(streamer);
      const getCurrentStream = await got.get(`https://api.twitch.tv/helix/streams?user_id=${streamer.data.id}`,{
        headers:{
          'Client-ID' :` ${process.env.CLIENTID}`,
          'Authorization': `Bearer ${token.data.access_token}`,
          'Accept' : 'application/vdn.twitchtv.v5+json'
        }
      });

      let stream = JSON.parse(getCurrentStream.body);
      console.log(stream);
      if(stream.data[0] !== undefined){
        return {
          code:200,
          data: {id: stream.data[0].id}/*{
          display_name: user.data[0].display_name,
          profile_image_url: user.data[0].profile_image_url
        }*/
        }
      } else {
        throw Error("The streamer isn't live right now");
      }
    }catch (error){
      console.error(error);
      return {code: 500, message: 'Error getting stream data', errCode: 10301}
    }

  }
}

module.exports = new StreamerController();
