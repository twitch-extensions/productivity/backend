var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');
const cors = require('cors');
require('dotenv').config();

var indexRouter = require('./routes/index');

var app = express();

/*var corsOptions = {
  origin: 'https://h4xzi91hk16buwt8iicnz8mrxelvd1.ext-twitch.tv/',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}*/

//app.use(cors(corsOptions));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  next();
});
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());


//app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

module.exports = app;
